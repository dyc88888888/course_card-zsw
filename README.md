# 长理教务后台系统
接口文档：https://www.eolinker.com/share/index?shareCode=jKkwue

* 下载运行时记得修改Mysql和Redis的密码
* 权限校验的url配置在yml文件

移动端代码: https://github.com/892681347/EduAdminSystem

后端代码: https://github.com/zhoushiwang99/course_card.git

启动：
`nohup java -jar course-card-0.0.1-SNAPSHOT.jar > course-card.log  2>&1 &`

dubug启动：
> 可以对服务器上的程序debug

dubug启动的话要在idea里面配置信息
`nohup java -jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 course-card-0.0.1-SNAPSHOT.jar > course-card.log  2>&1 & `

![img.png](img.png)

> 其中图片里的command餐数是  `-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005`

安装环境：
需要有redis、mysql、jdk、kafka(不必须，但是不安装来会打印许多日志)


然后就是要使用域名，这样换服务器不用重装app

日志的话，输出到了 `/var/log/carryshop` 一般要看日志的话在error里面

想实现自动更新软件的话，要安装nginx来配置服务器新的安装的目录。App启动的时候会去检查有没有新的安装包发布（mysql也要放条信息）
